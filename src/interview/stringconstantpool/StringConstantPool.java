package interview.stringconstantpool;

public class StringConstantPool {
    public static void main(String[] args) {
        String s1="Hello ";
        s1+="World";
        System.out.println(s1);
        String s2 = "Hello";
        s2 += " ";
        s2 +="World";
        System.out.println(s2);
        String s3 = s1.intern();
        String s4 = s2.intern();
        System.out.println(s3 == s4);
        System.out.println(s3.equals(s4));
    }
}
