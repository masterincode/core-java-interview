package interview.stringconstantpool;

public class EqualMethodOfStringAndStringBuffer {
    public static void main(String[] args) {

        System.out.println("=====String======");
        String s1 = "Hello";
        String s2 = "Hello";
        s2.concat(" World");
        System.out.println("String Address Check: " + (s1 == s2)); //true
        System.out.println("String Content Check: " + s1.equals(s2)); //true
        System.out.println("=====new String======");
        String s3 = new String("Hello");
        String s4 = new String("Hello");
        System.out.println("new String Address Check: " + (s3 == s4)); //false
        System.out.println("new String Content Check: " + s3.equals(s4)); //true
        System.out.println("=====StringBuffer======");
        StringBuffer s5 = new StringBuffer("Hello");
        StringBuffer s6 = new StringBuffer("Hello");
        System.out.println("new StringBuffer Address Check: " + (s5 == s6)); //false
        System.out.println("new StringBuffer Address Check: " + s5.equals(s6)); //false it is just reference equality
        System.out.println("new StringBuffer Content Check: " + s5.toString().equals(s6.toString())); // Content Check
        System.out.println("=====StringBuilder======");
        StringBuilder s7 = new StringBuilder("Hello");
        StringBuilder s8 = new StringBuilder("Hello");
        System.out.println("new StringBuilder Address Check: " + (s7 == s8)); //false
        System.out.println("new StringBuilder Address Check: " + s7.equals(s8)); //false it is just reference equality
        System.out.println("new StringBuilder Content Check: " + s7.toString().equals(s8.toString())); // Content Check

        System.out.println("String       with StringBuffer :  "+s1.equals(s5));
        System.out.println("String       with StringBuilder:  "+s1.equals(s7));
        System.out.println("StringBuffer with StringBuffer :  "+s5.equals(s7));

    }
}
