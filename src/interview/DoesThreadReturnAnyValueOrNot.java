package interview;

public class DoesThreadReturnAnyValueOrNot {
    public static void main(String[] args) {
        Runnable runnable1 = () ->
        {
            for (int i = 0; i < 10; i++) {
                System.out.println("-Thread 1--" + "-----Happy Engineers Day------");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        };
        Thread t1 = new Thread(runnable1);
        t1.start();
    }
    /*
  Steps to create a new Thread using Runnable :
  1. Create a Runnable implementer and implement run() method.
  2. Instantiate Thread class and pass the implementer to the Thread, Thread has a constructor which accepts Runnable instance.
  3. Invoke start() of Thread instance, start internally calls run() of the implementer. Invoking start(),
     creates a new Thread which executes the code written in run().
  Note:
  Difference between calling run() and start()
  1. Calling start(), creates a new Thread which executes the code written in run().
  2. Calling run() directly doesn’t create and start a new Thread,
     it will run in the same thread.
     To start a new line of execution, call start() on the thread.
  */
}
