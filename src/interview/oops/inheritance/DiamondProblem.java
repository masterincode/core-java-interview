package interview.oops.inheritance;

interface Super1 {
    public void demo();
    public default void details() {
        System.out.println("Super1 details");
    }
}
interface Super2 {
    public void demo();
    public default void details() {
        System.out.println("Super2 details");
    }
}

public class DiamondProblem implements Super1, Super2 {
    public static void main(String[] args) {
        DiamondProblem diamondProblem = new DiamondProblem();
        diamondProblem.demo();
        diamondProblem.details();
    }

    @Override
    public void demo() {
        System.out.println("====demo=====");
    }

    public void details() {
        System.out.println("DiamondProblem details");
        Super1.super.details();
        Super2.super.details();
    }
}


