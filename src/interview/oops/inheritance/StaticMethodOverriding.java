package interview.oops.inheritance;

// Superclass
class Base {

    // Static method in base class which will be hidden in subclass
    public static void display() {
        System.out.println("Static or class method from ==> Base");
    }

    // Non-static method which will be overridden in derived class
    public void print()  {
        System.out.println("Non-static or Instance method from ==> Base");
    }

    public final void printFinal()  {
        System.out.println("final Non-static or Instance method from ==> Base");
    }
}

// Subclass
class Derived extends Base {

    // This method hides display() in Base
    // @Override: You cannot override static method
    public static void display() {
        System.out.println("Static or class method from Derived");
    }

    // This method overrides print() in Base
    @Override
     public void print() {
          System.out.println("Non-static or Instance method from Derived");
    }

    //You cannot override final method [Compile Time Error]
    // public final void printFinal()  {
    //      System.out.println("final Non-static or Instance method from Base");
    // }
}


public class StaticMethodOverriding {
    public static void main(String[] args) {

        Base obj1 = new Derived();

        // As per overriding rules static method can not be overridden, it will
        // calls Base's display()
        obj1.display();
        System.out.println();
        // Here overriding works and Derive's print() is called
        obj1.print();
        System.out.println();
        Derived obj2  = new Derived();
        obj2.display();
        System.out.println();
        // Here overriding works and Derive's print() is called
        obj2.print();
        obj2.printFinal();

    }
}

