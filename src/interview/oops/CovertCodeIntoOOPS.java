package interview.oops;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

public class CovertCodeIntoOOPS {

    public static void main(String[] args) {

        Scanner sc  = new Scanner(System.in);
        System.out.println("Please enter the String");
        UserDetails userDetails = new UserDetails();
        userDetails.setUpserInput(sc.nextLine());

        if(!Validation.validator(userDetails.getUpserInput())) {
            System.out.println("Validation Failed");
            return;
        }
        Map<Character, Integer> duplicateMap = new HashMap<>();
        char[] ch = userDetails.getUpserInput().toCharArray();
        Integer count;
        for(Character c : ch) {
            count = duplicateMap.get(c);
            if(count == null) {
                duplicateMap.put(c, 1);
            } else {
                count++;
                duplicateMap.put(c, count);
            }
        }
        ShowDetails.showDetails(duplicateMap);
        System.out.println("==========Execution Completed==================");
        main(null);
    }
}
// I-PEA
@Data
class UserDetails {
    private String upserInput;
}
class Validation {

    public static final String REGEX = "^[a-z0-9]*$";
    //https://www.tutorialspoint.com/java/java_regular_expressions.htm
    public static boolean validator(String value) {
        //Null Check
        if(value == null)
            return false;
        //Length Check
        if(value.length() > 10)
            return false;
        //Special Character Check
        if(!Pattern.compile(REGEX).matcher(value).find())
            return false;

        return true;
    }
}
class ShowDetails {

    public static void showDetails(Map<Character, Integer> map) {
        map.forEach( (k, v) -> {
            if(v >= 2) {
                System.out.println("Key: "+k+"  Value: "+v);
            }
        });
    }
}
interface DuplicateValue {
    // findDuplicate count
}



