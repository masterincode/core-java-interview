//package interview.oops.polymorphism.overriding;
//
//
//class Parent {
//
//    public Parent() {
//    }
//
//    void test(Integer a) { // ArrayList<Integer> list
//        System.out.println("Inside Parent class Integer method"+a);
//    }
//
//}
//
//class Child extends Parent{
//
////    @Override
//    void test(Number a) { //List<Integer> list
//        System.out.println("Inside Child class Number method"+a);
//    }
//
////    @Override
//    void test(Long a) {
//        System.out.println("Inside Child class Long method"+a);
//    }
//
//}
//
//public class Main {
//
//    public static void main(String ar[]) {
//
//
//       Parent o1 = new Parent(); // CTE , You cannot create object of abstract classs
////
//        o1.test(10); // Parent
////        o1.test(10.0); // CTE , you need to cast to int
//        o1.test(-10); // Parent
////
//        Parent o2 = new Child();
////
//        o2.test(10); // Number
////        o2.test(10.0); // // CTE , you need to cast to int
//        o2.test(-10); // Number
////
////        Child o3 = new Parent(); // CTE
////
////        o3.test(10); //
////        o3.test(10.0); //
////        o3.test(-10); //
//    }
//}
//
