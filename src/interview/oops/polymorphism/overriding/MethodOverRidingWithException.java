package interview.oops.polymorphism.overriding;

import java.io.FileNotFoundException;
import java.io.IOException;

class Parent1 {

    /*
     * Things u cannot do:--
     *  Parent                |    Child
     *------------------------------------------------------------------
     *  No Exception          |  Any Unchecked Exception Allowed i.e RuntimeException, IllegalArgumentException      Note: Not allowed to declare CHECKED Exception
     *  Checked Exception     |  Same Exception, Subclass Exception, Any RunTimeException Exception,  No Exception   Note: Not allowed to declare Parent Exception
     *  UnChecked Exception   |  Same Exception, Subclass Exception, Any RunTimeException Exception,  No Exception   Note: Not allowed to declare Parent Exception
     * */

    public void saveData() {
        System.out.println("parent---saveData()");
    }

    /*  Any Method who call this method who is going to throw exception
     *  then calling method should handle with Try and catch
     */
    public void getMethod() throws FileNotFoundException {
        System.out.println("-----FileNotFoundException--In Parent");
    }

    /*In static method ,restriction in child class even though it is not overriding, It is method Hiding (Compiler say It is Inherited Methods)
     * Things u cannot do:--
     *  Parent      |    Child
     *------------------------------------------------------------------
     *  public      |    protected,private     Note: Compile Time Error
     *  protected   |    private               Note: Compile Time Error
     *  static      |    non -static           Note: Compile Time Error
     * */

    /*Parent class has Object return type and child can have Object or String return type
     * It throws Checked Exception
     * */
    public Object getName() throws IOException {
        System.out.println("Checked Exception in Parent IOException");
        return "bms";
    }

    public static void addNumber() {
        System.out.println("Parent--addNumber()-------static method u cannot override");
    }

    protected static void multiplyNumber() {
        System.out.println("Parent--multiplyNumber()--static method u cannot override");
    }
}

public class MethodOverRidingWithException extends Parent1 {

    // while overriding only unchecked exception are allowed here  ,dont write checked exception IOException,SQLException
    // RuntimeException is unchecked Exception
    @Override
    public void saveData() throws RuntimeException {
        System.out.println("Child---saveData()");
    }

    //Same, SubClass Exception ,  No exception are allowed here
    @Override
    public void getMethod() throws RuntimeException {

        System.out.println("RuntimeException--child----");
    }

    /*Parent class has Object return type and child can have Object or String return type
     * */
    @Override
    public String getName() {
        System.out.println("Checked Exception in Parent IOException1 and RuntimeException1 in child");
        return "bms";
    }

    // In static method u cannot make protected here, It should be public only
// You cannot override static method, but u can write same method without @Override annotation
//	@Override
    public static void addNumber() {
        System.out.println("--Child---addNumber-");
    }

    protected static void multiplyNumber() {
        System.out.println("---multiplyNumber----child----");
    }


    public static void main(String[] args) {

        Parent1 parent = new MethodOverRidingWithException();
//		MethodOverRidingWithException parent = new MethodOverRidingWithException();
        parent.saveData();
        parent.addNumber();
        parent.multiplyNumber();
        try {
            //Any Method who call this method who is going to throw exception
            //  then calling method should handle with Try and catch
            parent.getMethod();
            parent.getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
