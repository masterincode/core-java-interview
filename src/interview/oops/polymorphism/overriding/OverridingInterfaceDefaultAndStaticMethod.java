package interview.oops.polymorphism.overriding;

interface First {

    default void print() {
        System.out.println("First---print()");
    }

    static void display(){
        System.out.println("First---display()");
    }
}

interface Second {

    default void print() {
        System.out.println("Second---print()");
    }

    static void display(){
        System.out.println("Second---display()");
    }
}

public class OverridingInterfaceDefaultAndStaticMethod implements First, Second {

    @Override
    public void print() {
        First.super.print();
        Second.super.print();
        System.out.println("Custom Implementation");
    }

 // @Override : You cannot override static method
    static void display(){
        System.out.println("Child---display()");
    }

    public static void main(String[] args) {
        First obj1 = new OverridingInterfaceDefaultAndStaticMethod();
        Second obj2 = new OverridingInterfaceDefaultAndStaticMethod();
        obj1.print();
        System.out.println();
        obj2.print();
        System.out.println();
        First.display();
        System.out.println();
        Second.display();
        System.out.println();
        OverridingInterfaceDefaultAndStaticMethod.display();

    }
}
