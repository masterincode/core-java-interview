package interview.oops.polymorphism.overriding;

import java.util.ArrayList;
import java.util.List;

// Superclass
class FirstGeneration {

     // What is covariant type ?
     // Ans: Note that the return type of the overridden method in the subclass must be a subtype of the return type
     //      of the overridden method in the superclass.
     //      In other words, it must be more specific, and not less specific or unrelated.

    /*
     * Things u cannot do:--
     *  Parent                               |    Child
     *------------------------------------------------------------------
     *  public FirstGeneration covariant()   |  public SecondGeneration covariant()    Note: Allowed (Covariant Type Example)
     *  public List<Integer> getNumber()     |  public ArrayList<Integer> getNumber()  Note: Allowed (Covariant Type Example) but vice versa will give CTE
     *  public Object showObject()           |  public FirstGeneration  showObject()   Note: Allowed
     *  public Object showObject()           |  public SecondGeneration showObject()   Note: Allowed
     *  public FirstGeneration print()       |  public FirstGeneration print()         Note: Allowed
     *
     *  public FirstGeneration display()     |  public Object display()                Note: CTE (Child class should always have subclass, here Object is parent of all class)
     *  public String showObject()           |  public FirstGeneration showObject()    Note: CTE
     * */

    public FirstGeneration covariantExample() {
        System.out.println("Covariant Example in Parent Class");
        return new FirstGeneration();
    }

    public List<Integer> getNumber() {
        return new ArrayList<>();
    }

    // Non-static method which will be overridden in derived class
    public FirstGeneration print()  {
        System.out.println("Non-static or Instance method from ==> Parent");
        return new FirstGeneration();
    }

    // Non-static method which will be overridden in derived class
    public FirstGeneration print2()  {
        System.out.println("Non-static or Instance method from ==> Parent");
        return new FirstGeneration();
    }

    public Object showObject() {
        return "Hi";
    }

    public void printData() {
        System.out.println("Print Data");
    }

}

// Subclass
class SecondGeneration extends FirstGeneration {

    @Override
    public SecondGeneration covariantExample() {
        System.out.println("Covariant Example in Child Class");
        return new SecondGeneration();
    }

    @Override
    public ArrayList<Integer> getNumber() {
        return new ArrayList<>();
    }

    // This method overrides print() in Base
    @Override
    public FirstGeneration print() {
        System.out.println("Covariant Return type of type => Child");
        this.printData(); // You can directly call the Parent method
        return new SecondGeneration();
    }

    // CTE
//    @Override
//    public Object print2() {
//        return "";
//    }

    @Override
    public FirstGeneration showObject() {
        System.out.println("Covariant Return type of type => Object");
        return new FirstGeneration();
    }


}

public class CovariantReturnTypeInOverriding {
    public static void main(String[] args) {
        FirstGeneration obj1 = new SecondGeneration();
        obj1.print();
        obj1.showObject();
    }
}

