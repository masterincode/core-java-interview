package interview.oops.polymorphism.overloading;

public class OverrloadingExample extends Parent {
    String commonName;
    int childAge;

    @Override
    public String add(String a, String b) {
        System.out.println("-Child--override hua--");
        return "";
    }
    protected Boolean methodChild(String a, String b) {
        System.out.println("-Child--override- hua-");
        return true;
    }

    public static void main(String[] args) {
        /* dad is of Parent type ,but stores address of an object child(OverrloadingExample)
         * dad can only access Parent class variables and its method
         */
        Parent dad = new OverrloadingExample();
        /*Non static method hai child method called hoga
         *Static method hai to parent method called hoga
         */
        dad.add(null, null); //-Child--override hua--
        dad.commonName = "Daddy"; //Only Parent variables appears here

        OverrloadingExample child = new OverrloadingExample();
        child.add(null, null); // child method only shows
        child.commonName = "Bunty"; //Note : Only Children variables appears here
        child.show(null);
        child.show(10);
    }

}

class Parent {
    String commonName;
    int parentAge;

    //Note: When you are passing null
    //Use Case 1:  show(Object a) &&& show(String a)  .....String will get called
    //Use Case 2:  show(Object a) &&& show(Integer a) .....Integer will get called
    //Use Case 3:  show(String a) &&& show(Integer a) .....Compile Time Error.......ambiguity
    //Use Case 4:  show(Integer a) &&& show(Number a) .....Integer will get called

	//Note: When you are passing Number
    //Use Case 1:  show(Integer a) &&& show(Number a) .....Integer will get called
    //Use Case 2:  show(Integer a) &&& show(long a)   .....long will get called
    //Use Case 3:  show(Integer a) &&& show(Long a)   .....Compile Time Error.......ambiguity


	//Use Case 2:  show(Integer a) &&& show(int a) .....int will get called
	//Use Case 3:  show(long a) &&& show(int a) ........int will get called
	//Use Case 4:  show(Integer a) && show(long a) && show(int a) .....int will get called [Note: NO Compile Time Error....... No ambiguity]

    public void show(Object a) {
        System.out.println("-Parent--Object--"+a);
    }
    //	public void show(String a) {System.out.println("-Parent--String--");}
    public void show(Integer a) {
        System.out.println("-Parent--Integer--"+a);
    }
//    public void show(long a) {
//        System.out.println("-Parent--long--"+a);
//    }
    public void show(int a) {
        System.out.println("-Parent--int--"+a);
    }
    protected String add(String a, String b) {
        System.out.println("-Parent--Override --");
        return "";
    }
    protected Boolean methodParent(String a, String b) {
        System.out.println("---Parent Overrinding--");
        return true;
    }

}
