package interview.oops.encapsulation;

import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StudentRank {
    private int rank;
    private Student student;
}
