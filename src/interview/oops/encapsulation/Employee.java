package interview.oops.encapsulation;

import lombok.*;

import java.util.Objects;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Employee implements Comparable<Employee> {
    private int id;
    private String name;
    private String city;
    private int salary;
    private int age;

    @Override
    public int compareTo(Employee o) {
        if (this.getSalary() > o.getSalary()) {
            return 1;
        } else if (this.getSalary() == o.getSalary()) {
            return 0;
        }
        return -1;
    }

    @Override
    public boolean equals(Object o) {
        System.out.println("When Hash Collision will happen then only equals method get called");
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id &&
                Float.compare(employee.salary, salary) == 0 &&
                age == employee.age &&
                name.equals(employee.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, salary, age);
    }

    @Override
    protected void finalize()
    {
        System.out.println("Finalize method is called");
    }

}
