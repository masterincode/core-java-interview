package interview.oops.encapsulation;
import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EmployeeDepartment {
    private String name;
    private int salary;
    private String deptName;
}
