package interview.oops.encapsulation;

import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Student {
    private String name;
    private String regNo;
    private int totalMarks;
}
