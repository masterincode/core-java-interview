package interview.oops.encapsulation;

import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class College implements Comparable<College>
{
    private String name;
    private int id;
    private float salary;

    @Override
    public int compareTo(College o) {

        if(this.getId()>o.getId())
        {
            return -1;
        }
        else if(this.getId()==o.getId())
        {
            return 0;
        }
        return 1;
    }

}