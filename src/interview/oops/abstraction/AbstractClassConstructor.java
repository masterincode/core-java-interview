package interview.oops.abstraction;

abstract class Bacchi {
    int a;
    public Bacchi(int a) {
        System.out.println("abstract class constructor " + a);
        this.a = a;
    }
}

public class AbstractClassConstructor extends Bacchi {

    AbstractClassConstructor() {
        // It always access public constructor
	    // For private Constructor it shows C.T.E
        super(5);
        System.out.println("default Constructor");
    }

    public static void main(String[] args) {
        AbstractClassConstructor chi = new AbstractClassConstructor();
    }
}
/*OUTPUT:
   abstract class constructor 5
   default Constructor
 */