package interview;

import java.util.ArrayList;
import java.util.List;

public class FinalKeyWordExample {

    public static void main(String[] args) {
        FinalKeyWordExample exp = new FinalKeyWordExample();
        ArrayList list = new ArrayList();
        list.add(5);
        list.add(6);
        exp.output(list);
    }

    public void output(final int a) {
//        a = a + 2;  //Cannot assign a value to final variable 'a' C.T.E
    }

    public void output(final Integer a) {
//        a = a + 2; //Cannot assign a value to final variable 'a' C.T.E
    }

    public void output(final List<Integer> list) {
        list.add(10);
        System.out.println(list);
    }
}
