package interview.compiletimeerror;

import lombok.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ImmutableClass {

	public static void main(String args[]){

			Address address = Address.builder().city("Mumbai").zipCode("8000560").state("Maharashtra").build();
			Mazdoor m1 = new Mazdoor("Rohit",45, new ArrayList<>() ,address);
		    Mazdoor m2 = new Mazdoor("Dhoni",07, new ArrayList<>(),address);
			System.out.println(m1);
			System.out.println(m2);
			m1.getAddress().setCity("Patna");
		    m2.getAddress().setCity("Delhi");
			System.out.println(m1);
			System.out.println(m2);
		
		  //Immmutable class content cannot be changed thats y setter is not allowed
		    m1=m2;
		    System.out.println(m1.getName());   //Dhoni
		    System.out.println(m1.getMobile()); //07
	}
}

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
 class Address {
	private String street;
	private String city;
	private String state;
	private String zipCode;
}

@ToString
final class Mazdoor{
	
	private final String name;
	private final int mobile;
	private final List<String> toDoList;
	private final List<String> toolsList;
	private final Address address;
	
	public Mazdoor(String name, int mobile, List<String> toDoList,Address address,  String... toolsList) {
		super();
		this.name = name;
		this.mobile = mobile;
		//First way to create immutable collection object
		this.toDoList = Collections.unmodifiableList(toDoList);
		//Second way to create immutable collection object
		this.toolsList = Arrays.asList(toolsList);
		//
		this.address = new Address(address.getStreet(), address.getCity(), address.getState(), address.getZipCode());
	}
	
	//Note: When variable is final by default only getters will come 

	public String getName() {
		return name;
	}
	
//If you write setter , you will get compile time error
//	public void setName(String name){
//		this.name=name;
//	}

	public int getMobile() {
		return mobile;
	}

	public Address getAddress() {
		return new Address(address.getStreet(), address.getCity(), address.getState(), address.getZipCode());
	}
	
	
	
	
	
}
