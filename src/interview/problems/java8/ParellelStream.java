package interview.problems.java8;

import java.util.ArrayList;
import java.util.List;

public class ParellelStream {

    public static void main(String[] args) {

        List<Integer> numberList = new ArrayList<>(10000000);

        for (int i = 1; i <= 10000000; i++) {
            numberList.add(i);
        }
        numberList.parallelStream().forEach(System.out::println);
    }
}
