package interview.problems.java8;

import interview.oops.encapsulation.Employee;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class FindMaxAndMinSalaryUsingLambdaExpression {
    public static void main(String[] args) {

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(Employee.builder().name("Sachin").salary(1000).build());
        employeeList.add(Employee.builder().name("Virat").salary(5000).build());
        employeeList.add(Employee.builder().name("Dhoni").salary(3000).build());
        employeeList.add(Employee.builder().name("Dravid").salary(2000).build());
        employeeList.add(Employee.builder().name("Shubham").salary(4000).build());


        //Max salary
        Employee employee1 =  employeeList.stream().max(Comparator.comparingInt(Employee::getSalary)).get(); // Use get() to get data

        System.out.println("Max Salary: "+employee1);


        //Min salary
        Employee employee2 =  employeeList.stream().min(Comparator.comparingInt(Employee::getSalary)).get(); // Use get() to get data

        System.out.println("Min Salary: "+employee2);

        //Find Min number from arrayList
        Comparator<Integer> comparator = Comparator.comparing(Integer::intValue);

        Integer maximum = Stream.of(3,5,4,6,2,1).max(comparator).get();
        Integer minimum = Stream.of(6,5,4,3,2,1).min(comparator).get();
        System.out.println("Maximum: "+maximum+"  Minimum No: "+minimum);

    }
}
