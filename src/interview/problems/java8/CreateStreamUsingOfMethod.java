package interview.problems.java8;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CreateStreamUsingOfMethod {
    public static void main(String[] args) {
        Stream<Integer> numberStream = Stream.of(1,2,3,4,5);
        numberStream.forEach(System.out::println);

        Stream<String> stringStream = Stream.of("Sachin","Virat","Dhoni");
        stringStream.forEach(System.out::println);

        //Even Number
        List<Integer> evenNum = Stream.of(1,2,3,4,5,6).filter(num -> num % 2 == 0).collect(Collectors.toList());
        System.out.println(evenNum);

        long count = Stream.of(1,2,3,4,5,6).filter(num -> num % 2 == 0).count();
        System.out.println("Even number Count: "+count);
    }
}
