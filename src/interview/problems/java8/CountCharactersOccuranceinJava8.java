package interview.problems.java8;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CountCharactersOccuranceinJava8 {
    public static void main(String[] args) {
        String someString = "elephant";
        char someChar = 'e';
        int count = 0;

        // Java 8
        long charCount1 = someString.chars().filter(ch -> ch == 'e').count();
        long charCount2 = someString.codePoints().filter(ch -> ch == 'e').count();

        Map<Character, Long> vowelCount = someString.chars()
                                                    .filter(ch -> "aeiouAEIOU".indexOf(ch) != -1)
                                                    .mapToObj(ch -> (char) ch)
                                                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        System.out.println("Vowels and their count: " + vowelCount);


        //Regular Approach
        for (int i = 0; i < someString.length(); i++) {
            if (someString.charAt(i) == someChar) {
                count++;

            }
        }

        //HashMap
        HashMap<Character, Integer> hashmap = new HashMap<>();
        char[] ch = someString.toCharArray();
        for(char c: ch) {
            Integer counting = hashmap.get(c);
            if(counting == null) {
                hashmap.put(c, 1);
            } else {
                counting++;
                hashmap.put(c, counting);

            }

        }
        System.out.println(hashmap.get(someChar));
    }
}
