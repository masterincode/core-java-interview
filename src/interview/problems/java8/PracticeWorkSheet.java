package interview.problems.java8;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

class Emp {
    private int id;
    public Emp(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Emp)) return false;
        Emp emp = (Emp) o;
        return getId() == emp.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
public class PracticeWorkSheet {

    public static void main(String[] args) {

        Map<Emp, Integer> map = new HashMap<>();
        Emp e1 = new Emp(1);
        Emp e2 = new Emp(2);
        map.put(e1, 1);
        map.put(e2, 1);

        e2.setId(1);
        System.out.println(map.get(e2));

        Map<String, Integer> mapper = new HashMap<>();
        String s1 = "Virat";
        String s2 = "Sachin";
        mapper.put(s1, 1);
        mapper.put(s2, 2);
        s2 = "Virat";
        System.out.println(mapper.get(s2));


        List<Integer> list = new ArrayList<>();
        addValue(list);
        list =  new LinkedList<>();
        addValue(list);
        list =  new CopyOnWriteArrayList<>();
        addValue(list);
        list = new Stack<>();
        addValue(list);
        list = new Vector<>();
        addValue(list);


    }




    public static List<Integer> addValue(List<Integer> list) {
        list.add(100);
        return list;
    }
}
