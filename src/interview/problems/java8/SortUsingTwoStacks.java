package interview.problems.java8;

import java.util.Stack;

public class SortUsingTwoStacks {

    public static void main(String[] args) {
        Stack<Integer> firstStack = new Stack<>();
        firstStack.push(5);
        firstStack.push(2);
        firstStack.push(7);
        firstStack.push(1);
        firstStack.push(4);

        Stack<Integer> secondStack = new Stack<>();

        while (!firstStack.isEmpty()) {
            int temp = firstStack.pop();
            while (!secondStack.isEmpty() && secondStack.peek() > temp) {
                firstStack.push(secondStack.pop());
            }
            secondStack.push(temp);
        }

        while (!secondStack.isEmpty()) {
            System.out.println(secondStack.pop());
        }
    }
}

