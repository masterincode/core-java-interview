package interview.problems.java8;

import java.util.Arrays;
import java.util.List;

public class SumOfEvenAndOddNumber {
    //https://www.baeldung.com/java-stream-sum
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5);

        //Sum of Odd Number
        Integer oddSum = integers.stream().filter(num -> num % 2 != 0).reduce(0, Integer::sum);
        System.out.println("Sum Of Odd Numbers: "+oddSum);

        //Sum of Even Number
        Integer evenSum = integers.stream().filter(num -> num % 2 == 0).reduce(0, Integer::sum);
        System.out.println("Sum Of Even Numbers: "+evenSum);
    }
}
