package interview.problems.java8;

import interview.oops.encapsulation.EmployeeDepartment;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ConvertListToMap {

    public static void main(String[] args) {

        EmployeeDepartment e1 = EmployeeDepartment.builder().name("Virat").deptName("Batting").build();
        EmployeeDepartment e2 = EmployeeDepartment.builder().name("Rohit").deptName("Batting").build();
        EmployeeDepartment e3 = EmployeeDepartment.builder().name("Siraj").deptName("Bowling").build();
        EmployeeDepartment e4 = EmployeeDepartment.builder().name("Malik").deptName("Bowling").build();
        List<EmployeeDepartment> list = Arrays.asList(e1, e2, e3, e4);


        Map<String, List<EmployeeDepartment>> map = list.stream().collect(Collectors.groupingBy(EmployeeDepartment::getDeptName));
        System.out.println("=============================");
        map.values().forEach(val -> {
            System.out.println("Val: "+val);
        });
        map.keySet().forEach(key -> {
            System.out.println("key: "+key);
        });
        System.out.println("=============================");
        map.forEach((k, v) -> {
            System.out.println("Key: "+k+"  Value: "+v);
        });
    }
}

