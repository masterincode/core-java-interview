package interview.problems.java8;

import interview.oops.encapsulation.Employee;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortListElementInAscDesc {
    public static void main(String[] args) {

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(Employee.builder().name("Sachin").city("Mumbai").salary(1000).build());
        employeeList.add(Employee.builder().name("Virat").city("New Delhi").salary(5000).build());
        employeeList.add(Employee.builder().name("Dhoni").city("Ranchi").salary(5000).build());
        employeeList.add(Employee.builder().name("Jadeja").city("Kutch").salary(2000).build());
        employeeList.add(Employee.builder().name("Hardik").city("Kutch").salary(2000).build());
        employeeList.add(Employee.builder().name("Malik").city("Kashmir").salary(3000).build());

        //5. Sort By City and Name ASC(If salary is same then sort by Name)
        Comparator<Employee> sortByCityAndNameComparator1 = (e1, e2) ->   (e1.getCity().compareTo(e2.getCity())) > 0 ?  1 :
                                                                         (e1.getCity().compareTo(e2.getCity())) < 0 ? -1 :
                                                                         (e1.getName().compareTo(e2.getName()));
        // TODO: Default ordering is ASC, to make it DESC then use .reverse()
        Comparator<Employee> sortByCityAndNameComparator2 = Comparator.comparing(Employee::getCity).thenComparing(Employee::getName).reversed();

        List<Employee> sortByCityAndNameList = employeeList.stream().sorted(sortByCityAndNameComparator2).collect(Collectors.toList());
        System.out.println("sortByCityAndNameList: "+sortByCityAndNameList.stream().map(Employee::getName).collect(Collectors.toList()));
        System.out.println();


        //4. Sort By Salary and Name ASC(If salary is same then sort by Name)
        Comparator<Employee> sortBySalaryAndNameComparator = (emp1, emp2) -> (emp1.getSalary() > emp2.getSalary()) ? 1 :
                                                                             (emp1.getSalary() < emp2.getSalary()) ? -1 :
                                                                             (emp1.getName().compareTo(emp2.getName()));

        List<Employee> sortBySalaryAndNameList = employeeList.stream().sorted(sortBySalaryAndNameComparator).collect(Collectors.toList());
        System.out.println("sortBySalaryAndNameList: "+sortBySalaryAndNameList.stream().map(Employee::getName).collect(Collectors.toList()));
        System.out.println();


        //3. Sort by Name ASC
        Comparator<Employee> comparator = Comparator.comparing(Employee::getName).reversed();

        List<Employee> sortedEmployeeList = employeeList.stream().sorted(comparator).collect(Collectors.toList());

        System.out.println("Result: "+sortedEmployeeList.stream().map(Employee::getName).collect(Collectors.toList()));
        System.out.println();


        //2. Sorted Salary DESC
        List<Employee> employeeSortedListDesc = employeeList.stream().sorted((emp1, emp2) -> (emp1.getSalary() > emp2.getSalary() )? -1 :
                                                                                             (emp1.getSalary() < emp2.getSalary()) ?  1 : 0 )
                                                                                             .collect(Collectors.toList());
        System.out.println(employeeSortedListDesc);
        System.out.println();


        //1. Sorted Salary ASC
        List<Employee> employeeSortedListAsc = employeeList.stream().sorted((emp1, emp2) -> (emp1.getSalary() > emp2.getSalary() )?  1 :
                                                                                            (emp1.getSalary() < emp2.getSalary()) ? -1 : 0 )
                                                                                            .collect(Collectors.toList());
        System.out.println(employeeSortedListAsc);







    }
}
