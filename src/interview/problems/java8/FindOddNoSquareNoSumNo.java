package interview.problems.java8;

import java.util.Arrays;
import java.util.List;

public class FindOddNoSquareNoSumNo {
    public static void main(String[] args) {

        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5);

        long sum = integers.stream().filter(num -> num % 2 != 0)
                                    .map(num -> num * num)
                                    .reduce(0, Integer::sum);
        System.out.println(sum);
    }
}
