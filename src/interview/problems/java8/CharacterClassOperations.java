package interview.problems.java8;

public class CharacterClassOperations {

    public static void main(String[] args) {

     // 1. Convert String to char

     String val1= "a";
     char op1 = val1.charAt(0);
     System.out.println("Convert String to char: "+op1);

     // 2. Convert char to String
     String val2 = String.valueOf(op1);
     System.out.println("Convert char to String: "+val2+ "  Length: "+val2.length());

     String val3 = "" + op1;
     System.out.println("Add char to Empty String: "+val3+ "  Length: "+val3.length());

     // 1. Check character is Number
     System.out.println("isNumber: "+Integer.valueOf("150"));
     System.out.println("isNumber: "+Character.isLetter("a".charAt(0)));



    }
}
