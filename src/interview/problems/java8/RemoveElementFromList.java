package interview.problems.java8;

import interview.oops.encapsulation.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RemoveElementFromList {

    public static void main(String[] args) {

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(Employee.builder().name("Sachin").city("Mumbai").salary(1000).build());
        employeeList.add(Employee.builder().name("Virat").city("New Delhi").salary(5000).build());
        employeeList.add(Employee.builder().name("Dhoni").city("Ranchi").salary(5000).build());
        employeeList.add(Employee.builder().name("Jadeja").city("Kutch").salary(2000).build());
        employeeList.add(Employee.builder().name("Hardik").city("Kutch").salary(2000).build());
        employeeList.add(Employee.builder().name("Malik").city("Kashmir").salary(3000).build());

        System.out.println("removeElementUsingFilter:   "+removeElementUsingFilter(employeeList).stream().map(Employee::getName).collect(Collectors.toList()));
        System.out.println("removeElementUsingRemoveIf: "+removeElementUsingRemoveIf(employeeList).stream().map(Employee::getName).collect(Collectors.toList()));
    }

    // Time Complexity: O(n)
    // Space Complexity: O(n)
    public static List<Employee> removeElementUsingFilter(List<Employee> employeeList) {
        return employeeList.stream().filter(employee -> !employee.getName().equals("Malik")).collect(Collectors.toList());
    }

    // Time Complexity: O(n)
    // Space Complexity: O(1)
    // removeIf method of ArrayList which internally uses an efficient algorithm to remove the elements in-place.
    public static List<Employee> removeElementUsingRemoveIf(List<Employee> employeeList) {
        employeeList.removeIf(person -> person.getName().equals("Malik") && person.getCity().equals("Kashmir"));
        return employeeList;
    }
}
