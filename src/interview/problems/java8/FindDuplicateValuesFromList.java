package interview.problems.java8;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

public class FindDuplicateValuesFromList {

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(2);
        arrayList.add(4);
        arrayList.add(3);

        System.out.println("Duplicate elements in ArrayList are :");
        arrayList.stream()
                .filter(num -> Collections.frequency(arrayList, num) > 1)
                .distinct()
                .forEach(System.out::println);

        Teacher t1 = Teacher.builder().id(1).name("Virat").build();
        Teacher t2 = Teacher.builder().id(1).name("Virat").build();
        Teacher t3 = Teacher.builder().id(2).name("Rohit").build();
        Teacher t4 = Teacher.builder().id(3).name("Sachin").build();
        Teacher t5 = Teacher.builder().id(3).name("Sachin").build();
        List<Teacher> teacherList = Arrays.asList(t1, t2, t3, t4, t5);

        Map<Integer, List<Teacher>> employeeByIdMap = teacherList.stream().collect(Collectors.groupingBy(Teacher::getId));

        employeeByIdMap.values().stream()
                .filter(employeesList -> employeesList.size() > 1)
                .flatMap(Collection::stream)
                .forEach(System.out::println);


    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
class Teacher {
    private int id;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Teacher)) return false;
        Teacher teacher = (Teacher) o;
        return getId() == teacher.getId() && Objects.equals(getName(), teacher.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }
}
