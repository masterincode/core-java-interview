package interview.problems.java8;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RemoveDuplicateUsingJava8 {
    public static void main(String[] args) {

        // 1. Using .distinct() Operator
        List<Integer> duplicateNumber = Arrays.asList(5, 0, 3, 1, 2, 3, 0, 0);
        List<Integer> uniqueNumber = duplicateNumber.stream().distinct().collect(Collectors.toList());
        System.out.println("Distinct Number: "+uniqueNumber);

        List<String> duplicateString = Arrays.asList("Rohit", "Rahul","Rohit","Rahul","rohit");
        List<String> uniqueString = duplicateString.stream().distinct().collect(Collectors.toList());
        System.out.println("Distinct String: "+uniqueString);

        // Using Set
        Set<String> uniqueSet = duplicateString.stream().collect(Collectors.toSet());
        System.out.println("Distinct String Using Set: "+uniqueSet);
    }
}
