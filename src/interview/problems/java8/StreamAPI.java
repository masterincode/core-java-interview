package interview.problems.java8;

import interview.oops.encapsulation.Employee;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamAPI {
    public static void main(String[] args) {

        Arrays.asList("a1", "a2", "a3")
                .stream()
                .findFirst()
                .ifPresent(System.out::println);  // a1

        List<String> myList = Arrays.asList("Sehwag", "a2", "Bunty", "Sachin", "c1");
        myList
                .stream()
                .filter(s -> s.startsWith("S"))
                .map(String::toUpperCase)
                .sorted()
                .forEach(System.out::println);

        // Problem 2

        Employee e1 = Employee.builder().name("akshat").build();
        Employee e2 = Employee.builder().name("Arvind").build();
        Employee e3 = Employee.builder().name("Anand").build();
        Employee e4 = Employee.builder().name("avinash").build();
        Employee e5 = Employee.builder().name("bms").build();
        Employee e6 = Employee.builder().name("Satish").build();
        Employee e7 = Employee.builder().name("akshat").build();


        List<Employee> list = Arrays.asList(e1,e2,e3,e4,e5,e6,e7);

        //1. Execute till filter Only
        Stream<Employee> streamFilter =  list.stream().filter(emp -> emp.getName().toLowerCase().startsWith("a"));
        streamFilter.forEach(System.out::println);
        System.out.println();

        // 2. Execute till Map Only
        Stream<String> stream =  list.stream().filter(emp -> emp.getName().toLowerCase().startsWith("a"))
                .map(Employee::getName);
        stream.forEach(System.out::println);
        System.out.println();

        // 3. Execute till List
        List<String> newList = list.stream().filter(emp -> emp.getName().toLowerCase().startsWith("a"))
                .map(Employee::getName)
                .distinct()
                .collect(Collectors.toList());
        System.out.println(newList);
    }
}
