package interview.problems.java8;

import interview.oops.encapsulation.EmployeeDepartment;

import java.util.ArrayList;
import java.util.List;

public class SumOfSalaryDeptwise {
    public static void main(String[] args) {

        List<EmployeeDepartment> employeeList = new ArrayList<>();
        employeeList.add(new EmployeeDepartment("Sachin",1000, "IT"));
        employeeList.add(new EmployeeDepartment("Virat",5000, "IT"));
        employeeList.add(new EmployeeDepartment("Dhoni",3000, "IT"));
        employeeList.add(new EmployeeDepartment("Dravid",2000, "FINANCE"));

        long totalSalaryIT = employeeList.stream()
                                         .filter(employee -> employee.getDeptName().equals("IT"))
                                         .map(EmployeeDepartment::getSalary)
                                         .reduce(0, Integer::sum);

        System.out.println("Total Salary of IT: "+totalSalaryIT);
    }
}
