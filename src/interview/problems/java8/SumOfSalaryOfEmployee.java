package interview.problems.java8;

import interview.oops.encapsulation.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SumOfSalaryOfEmployee {
    public static void main(String[] args) {

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(Employee.builder().name("Sachin").salary(1000).build());
        employeeList.add(Employee.builder().name("Virat").salary(5000).build());
        employeeList.add(Employee.builder().name("Dhoni").salary(3000).build());
        employeeList.add(Employee.builder().name("Dravid").salary(2000).build());

        // Using summingInt()
        int totalSalary1 = employeeList.stream()
                .collect(Collectors.summingInt(Employee::getSalary));
        System.out.println("Total Salary Using summingInt() : "+totalSalary1);

        // Using reduce()
        long totalSalary2 = employeeList.stream()
                                        .map(employee -> employee.getSalary()) // Only returning salary
                                        .reduce(0,Integer::sum);

        System.out.println("Total Salary Using reduce() : "+totalSalary2);
    }
}
