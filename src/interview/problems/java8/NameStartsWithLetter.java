package interview.problems.java8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NameStartsWithLetter {
    public static void main(String[] args) {

        List<String> list1 = Arrays.asList("Sehwag","Anurag","Sachin","Raju");
        List<String> list2 = list1.stream().filter( name -> name.toLowerCase().startsWith("s")) // It is case sensitive
                .collect(Collectors.toList());
        System.out.println(list2);
    }
}
