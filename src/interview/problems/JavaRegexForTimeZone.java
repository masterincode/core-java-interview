package interview.problems;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class JavaRegexForTimeZone {

    private static final String DATE_FORMAT = "^[0-9]{4}[-][0-9]{2}[-][0-9]{2}$";
    private static final String DATE_TIME_SECOND = "^[0-9]{4}[-][0-9]{2}[-][0-9]{2}[T][0-9]{2}[:][0-9]{2}[:][0-9]{2}+$";
    private static final String DATE_TIME_SECOND_MILISECOND = "^[0-9]{4}[-][0-9]{2}[-][0-9]{2}[T][0-9]{2}[:][0-9]{2}[:][0-9]{2}[.][0-9]+$";
    private static final String DATE_TIME_ZONE = "^[0-9]{4}[-][0-9]{2}[-][0-9]{2}[T][0-9]{2}[:][0-9]{2}[:][0-9]{2}[.][0-9]{3}[Z]$";
    private static final String DATE_TIME_UTC_ZONE = "^[0-9]{4}[-][0-9]{2}[-][0-9]{2}[T][0-9]{2}[:][0-9]{2}[:][0-9]{2}[.][0-9]{9}[-+][0-9]{2}[:][0-9]{2}$";

    public static void main(String[] args) {

        List<String> dateTimeUseCaseList = new ArrayList<>();

        dateTimeUseCaseList.add("2021-01-01");
        dateTimeUseCaseList.add("2021-10-31");
        dateTimeUseCaseList.add("442021-11-30555");
        dateTimeUseCaseList.add("2021-11-30T11:50:55");
        dateTimeUseCaseList.add("2021-09-03T20:04:58.495681");
        dateTimeUseCaseList.add("2021-09-03T19:31:36.073547100"); //LocalDateTime.now()
        dateTimeUseCaseList.add("2021-11-30T11:50:55.421Z");
        dateTimeUseCaseList.add("2021-11-30T11:50:55.421ZZZ");
        dateTimeUseCaseList.add("42342021-11-30T11:50:55.421ZSDDF");
        dateTimeUseCaseList.add("2021-09-03T19:31:36.073547100+05:30"); //OffsetDateTime.now()
        dateTimeUseCaseList.add("2021-09-03T19:31:36.073547100-05:30");
        dateTimeUseCaseList.add("2021-09-03T19:31:36.073547100+12:00");
        dateTimeUseCaseList.add("2021-09-03T19:31:36.073547100-00:00");
        dateTimeUseCaseList.add("2021-09-03T19:31:36.073547100+00:00");


        dateTimeUseCaseList.stream().forEach( dateTimeUseCase -> {
            if(checkRegexPattern(dateTimeUseCase) != null )
            System.out.println(dateTimeUseCase+ " " +"=>>> "+checkRegexPattern(dateTimeUseCase));
        });
        System.out.println();
        System.out.println(LocalDateTime.now());
        System.out.println(OffsetDateTime.now());

    }

    public static LocalDate checkRegexPattern(String targetDate) {

       try {
           if(Pattern.compile(DATE_FORMAT).matcher(targetDate).find()) {
               return LocalDate.parse(targetDate);
           }
           if(Pattern.compile(DATE_TIME_SECOND).matcher(targetDate).find()) {
               return LocalDateTime.parse(targetDate).toLocalDate();
           }
           if(Pattern.compile(DATE_TIME_SECOND_MILISECOND).matcher(targetDate).find()) {
               return LocalDateTime.parse(targetDate).toLocalDate();
           }
           if(Pattern.compile(DATE_TIME_ZONE).matcher(targetDate).find()) {
               return OffsetDateTime.parse(targetDate).toLocalDate();
           }
           if(Pattern.compile(DATE_TIME_UTC_ZONE).matcher(targetDate).find()) {
               return OffsetDateTime.parse(targetDate).toLocalDate();
           }
       }
       catch (DateTimeException e) {
           System.out.println(e.getMessage());
       }
       return null;
    }
}
