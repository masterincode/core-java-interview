package interview.problems;
import interview.oops.encapsulation.Student;
import interview.oops.encapsulation.StudentRank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ShowRankOfStudent {
    public static void main(String[] args) {

        List<Student> studentList =  new ArrayList<>();
        studentList.add(new Student("Sachin", "A125", 350));
        studentList.add(new Student("Sachin", "A192", 750));
        studentList.add(new Student("Sachin", "A156", 950));
        studentList.add(new Student("Sachin", "A135", 650));

        List<Student> studentSortedList = studentList.stream().sorted((student1, student2) -> (student1.getTotalMarks() > student2.getTotalMarks()) ? -1 :
                (student1.getTotalMarks() < student2.getTotalMarks()) ?  1 : 0 )
                .collect(Collectors.toList());

        List<StudentRank> studentRankList = new ArrayList<>();
        int rank = 1;
        for(Student student: studentSortedList)
        {
            studentRankList.add(new StudentRank(rank, student));
            rank++;
        };

        HashMap<String,StudentRank> map = new HashMap<>();
        studentRankList.forEach( student -> map.put(student.getStudent().getRegNo(), student)) ;
        map.forEach((k, v) ->  System.out.println("Key: "+k+" Value: "+v));

        //System.out.println(studentRankList.toString());

    }
}
