package interview.problems;

class Parent {

    public void walk(){
        System.out.println("Parent--walk"); //1
    }
    public void run(){
        walk();
        System.out.println("Parent--run"); //2
    }
}
class Child extends Parent {

    @Override
    public void walk(){
        super.walk();
        System.out.println("Child--walk");
    }

    @Override
    public void run(){
        super.run();
        System.out.println("Child--run");
    }

}
public class MethodCalling {
    public static void main(String[] args) {
        Parent p = new Child();
        p.run();
    }
}
