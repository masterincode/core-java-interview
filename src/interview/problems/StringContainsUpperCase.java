package interview.problems;

public class StringContainsUpperCase {

    public static void main(String[] args) {

      String str = "thisIsAVariable";

      char[] ch = str.toCharArray();
      String result = "";

      for (char c: ch) {

          if(Character.isUpperCase(c)) {
              result = result + '_';
          }
          result = result + c;
      }
        System.out.println(result);

    }
}
