package interview.problems;

public class TypePromotion {

    public static void main(String[] args) {

        int a = 10;
        System.out.println(a + 2L); // int + long
        System.out.println(a + 2f); // int + float
        System.out.println(a + 2d); // int + double

        System.out.println('j' + 2); // char + int = ASCII code

    }
}
