package interview.problems;

import java.util.Scanner;
import java.util.regex.Pattern;

public class JavaRegex {
    public static void main(String[] args) {
        /*
        * ^                       Start Anchor
        * (?=.*[A-Z])             Ensure string has 1 upppercase letters.
        * (?=.*[A-Z]).*[A-Z])     Ensure string has 2 upppercase letters.
        * (?=.*[!@#$&*])          Ensure string has 1 special case letters.
        * (?=.*[0-9])             Ensure string has 1 digit.
        * (?=.*[0-9].*[0-9])      Ensure string has 2 digits.
        * (?=.*[a-z])             Ensure string has 1 lowercase letters.
        * (?=.*[a-z].*[a-z])      Ensure string has 2 lowercase letters.
        * .{8}                    Ensure string is of length 8.
        * .{8,}                   Ensure string is of length >= 8.
        * $
        */
        // Final Regex
        String REGEX = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$&*]).{8,}$";
        String regex = "^[A-Z][a-z0-9_]*$";

        Scanner sc  = new Scanner(System.in);
        System.out.println("Please enter the String");

        if(Pattern.compile(REGEX).matcher(sc.nextLine()).find())
          System.out.println("Proper Data !!!");
        else
            System.out.println("Wrong data !!!");
        System.out.println("====================================================");
        main(null);
    }
}
