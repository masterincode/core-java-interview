package interview.problems;

import java.util.Scanner;

public class BinarySearchToFindWordInArray {

    public static void main(String[] args) {
        String[] words = {"apple", "banana", "cherry", "date", "elderberry"};
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the word to be Search");
        String target = sc.nextLine();
        int index = binarySearch(words, target);
        if (index >= 0) {
            System.out.println("Found at index " + index);
        } else {
            System.out.println("Not found");
        }
        System.out.println();
        main(null);
    }

    public static int binarySearch(String[] array, String target) {
        int firstIndex = 0;
        int lastIndex = array.length - 1;

        while (firstIndex <= lastIndex) {
            int middle = (firstIndex + lastIndex) / 2;

            if (array[middle].equals(target)) {
                return middle;
            }
            // 1. "a".comparaTo("b") => -1
            // 2. "b".comparaTo("a") =>  1
            else if (array[middle].compareTo(target) < 0) {
                firstIndex = middle + 1;
            } else {
                lastIndex = middle - 1;
            }
        }

        return -1; // target not found
    }


}
