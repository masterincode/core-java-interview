package interview.problems;

import java.util.Arrays;
import java.util.Comparator;

public class Sort2DArray {

    public static void main(String[] args) {

        int[][] arr = {{78,5,6},{49,45,63},{12,89,53}};

        for (int[] ar: arr)
            System.out.println(Arrays.toString(ar));

        for (int[] a: arr)
            Arrays.sort(a);

        System.out.println();

//        for (int[] ar: arr)
//            System.out.println(Arrays.toString(ar));
        // Sort by row (ascending order)
        Arrays.sort(arr, Comparator.comparingInt(a -> a[0]));

        for (int[] ar: arr)
            System.out.println(Arrays.toString(ar));

// Sort by column (ascending order)
//        Arrays.sort(arr, Comparator.comparingInt(a -> a[0][columnIndex]));

    }
}
