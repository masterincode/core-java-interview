package interview.problems;

import interview.oops.encapsulation.Employee;

public class PassingObjectFromOneClassToAnother {
    public static void main(String[] args) {

        Employee employee = Employee.builder().name("Sachin").salary(1000).build();
        System.out.println("1: " + employee.toString());
        ThirdParty thirdParty = new ThirdParty();
        thirdParty.showDetails(employee);
        //show(employee);
        System.out.println("Final O/P: " + employee.toString());

    }

    public static void show(Employee employee) {
        employee.setName("Vikas");
        System.out.println("4: " + employee.toString());
        employee = Employee.builder().name("Dhoni").salary(5000).build(); // Never workans will be Vikas
        System.out.println("5: " + employee.toString());
    }
}

class ThirdParty {

    public void showDetails(Employee employee) {
        employee.setName("ThirdParty");
        System.out.println("2: " + employee.toString());
        employee = Employee.builder().name("Dhoni").salary(5000).build();// Never work ans will be ThirdParty
        System.out.println("3: " + employee.toString());
    }
}


