package interview.problems;

import java.util.*;

public class DataStructure {

    public static void main(String[] args) {
        /*An ArrayDeque provides constant time complexity for adding and removing elements from both the front and the back of the deque,
         making it a very efficient data structure for certain use cases.
         It is also thread-safe for concurrent use by multiple threads.*/

        Deque<Integer> queue = new ArrayDeque<>();
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);
        queue.add(6);
        queue.add(7);

        queue.addFirst(1);
        queue.addLast(8);

        queue.poll(); // Remove Element at 0 Index
        queue.pop(); // Remove Element at 0 Index

        queue.remove(); // Remove Element at 0 Index
        queue.remove(5); // Remove the element with value 5
        queue.removeFirst();
        queue.removeLast();

        //--------- Convert List into Array-----------------------------------------------
        List<String> arrayToList= Arrays.asList("Sachin","Dravid","Rahul","Yuvraj","Amit","Bunty");

        // 1. Using List toArray() method
        String[] arr = arrayToList.toArray(new String[0]);
        System.out.println(Arrays.toString(arr));

        // 2. Using Stream toArray() method
        String[] array = arrayToList.stream().toArray(String[]::new);
        System.out.println(Arrays.toString(array));

        //Remove Odd Number using Java 8 in Arraylist
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        list.removeIf(i -> i % 2 != 0);
        System.out.println(list);

    }
}

