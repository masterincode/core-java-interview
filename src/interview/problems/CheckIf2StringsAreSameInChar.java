package interview.problems;

import java.util.Arrays;

public class CheckIf2StringsAreSameInChar {
    public static void main(String[] args) {
        String s1 = "test";
        String s2 = "estt";
        int rightCheckCount = 0;
        int length = 0;

        if (s1.length() != s1.length())
            return;
        if (s1.length() == s1.length())
            length = s1.length();

        char[] c1 = s1.toCharArray();
        char[] c2 = s2.toCharArray();

        Arrays.sort(c1);
        Arrays.sort(c2);

        for (int i = 0; i < length; i++) {
            if (c1[i] == c2[i])
                rightCheckCount++;
        }
        System.out.println(rightCheckCount == length);
    }
}
