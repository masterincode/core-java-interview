package interview.problems;

import java.util.Scanner;

public class BinarySearchToFindCharInArray {

    public static void main(String[] args) {
        char[] words = {'a','b','c','d','e','f'};
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the character to be Search");
        char target = sc.nextLine().charAt(0);
        int index = binarySearch(words, target);
        if (index >= 0) {
            System.out.println("Found at index " + index);
        } else {
            System.out.println("Not found");
        }
        System.out.println();
        main(null);
    }

    public static int binarySearch(char[] array, char target) {
        int firstIndex = 0;
        int lastIndex = array.length - 1;

        while (firstIndex <= lastIndex) {
            int middle = (firstIndex + lastIndex) / 2;

            if (array[middle] == target) {
                return middle;
            } else if (array[middle] < target) {
                firstIndex = middle + 1;
            } else {
                lastIndex = middle - 1;
            }
        }

        return -1; // target not found
    }
}
