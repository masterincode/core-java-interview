package interview.problems;

public class SumOfDiagonalNumber {

    public static void main(String[] args) {

        int[][] arr = { {1, 2, 3, 4},
                        {5, 6, 7, 8},
                        {9,10,11,12},
                        {13,14,15,16}};


        int i = 0;
        int j = arr.length - 1;
        int sum = 0;

        //                        [i][i] -> [i][i] -> [i][i] -> [i][i]
        // Left To Right Diagonal [0][0] -> [1][1] -> [2][2] -> [3][3]
        // Right To Left Diagonal [0][3] -> [1][2] -> [2][1] -> [3][0]
        //                        [i][j] -> [i][j] -> [i][j] -> [i][j]
        while(i < arr.length && j >= 0) {
            sum = sum + arr[i][i] + arr[i][j];
            i++;
            j--;
        }

        System.out.println("Output: "+sum);
    }
}
