package interview.problems;

public class Print1To100WithoutAnyLoop {
    public static void main(String[] args) {
        printNos(10);
    }

    // Prints numbers from 1 to n
    public static void printNos(int n) {
        if (n > 0) {
            printNos(n - 1);
            System.out.print(n + " ");
        }
        return;
    }
}
