package interview.problems;

import java.util.Scanner;

public class BinarySearchToFindNumberInJava {

    public static void main(String[] args) {

        int arr[] = {10, 25, 45, 50, 54, 62, 78, 80, 90};

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number to be Search");
        int target = sc.nextInt();
		int index = binarySearch(arr, target);
		if (index >= 0) {
			System.out.println("Found at index " + index);
		} else {
			System.out.println("Not found");
		}
		System.out.println();
		main(null);
    }

	public static int binarySearch(int[] array, int target) {
		int firstIndex = 0;
		int lastIndex = array.length - 1;

		while (firstIndex <= lastIndex) {
			int middle = (firstIndex + lastIndex) / 2;

			if (array[middle] == target) {
				return middle;
			} else if (array[middle] < target) {
				firstIndex = middle + 1;
			} else {
				lastIndex = middle - 1;
			}
		}

		return -1; // target not found
	}
}
