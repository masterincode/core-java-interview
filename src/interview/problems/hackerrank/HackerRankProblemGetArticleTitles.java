package interview.problems.hackerrank;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


class Result {

    /*
     * Complete the 'getArticleTitles' function below.
     *
     * The function is expected to return a STRING_ARRAY.
     * The function accepts STRING author as parameter.
     *
     * URL for cut and paste:
     * https://jsonmock.hackerrank.com/api/articles?author=<authorName>&page=<num>
     *
     */

    // TODO This method Developer has to write to process Logic
    public static List<String> getArticleTitles(String author) throws IOException {
        String jsonString = getAPICall(author);
        System.out.println("Author Name: "+author+ ", jsonString: "+jsonString);
        List<Author> authorList = getBookTitleList(jsonString);
        List<String> bookTitleList = new ArrayList<>();
        for(Author author1: authorList) {
            if(author1.getTitle() != null) {
                bookTitleList.add(author1.getTitle());
            } else {
                bookTitleList.add(author1.getStoryTitle());
            }
        }
        System.out.println("bookTitleList: "+bookTitleList);
        return bookTitleList;
    }

    // TODO This method Developer has to write to List Object
    private static List<Author> getBookTitleList(String jsonString) {
        List<Author> authorList = new ArrayList<>();
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
            JSONArray  jsonArray  = (JSONArray) jsonObject.get("data");
            for(int i =0; i< jsonArray.size(); i++) {

               JSONObject localJsonObject = ((JSONObject) jsonArray.get(i));
               String title = (String)localJsonObject.get("title");
               String storyTitle = (String)localJsonObject.get("story_title");
               Author author = new Author(title, storyTitle);
               authorList.add(author);
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        return authorList;
    }

    static class Author {
        private String title;
        private String storyTitle;

        public Author(String title, String storyTitle) {
            this.title = title;
            this.storyTitle = storyTitle;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStoryTitle() {
            return storyTitle;
        }

        public void setStoryTitle(String storyTitle) {
            this.storyTitle = storyTitle;
        }
    }

    // TODO This method Developer has to write to call API
    public static String getAPICall(String authorName) throws IOException {
        // URL of the API endpoint
        // System.out.println("Author Name: "+authorName);
        String apiUrl = "https://jsonmock.hackerrank.com/api/articles?author="+authorName+"&page=1";

        // Create a URL object from the API endpoint
        URL url = new URL(apiUrl);

        // Open an HTTP connection to the URL
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        // Read the response from the connection
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        StringBuilder responseBuilder = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            responseBuilder.append(line);
        }
        reader.close();
        return responseBuilder.toString();
    }

}

public class HackerRankProblemGetArticleTitles {
    public static void main(String[] args) throws IOException {
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
//
//        String author = bufferedReader.readLine();
        List<String> authorNameList = Arrays.asList("epaga", "saintamh", "coloneltcb");
        for (String author: authorNameList) {
            List<String> result = Result.getArticleTitles(author);
            System.out.println("---------Final Result------------");
            result.forEach(System.out::println);
            System.out.println();
            System.out.println();
        }



//        bufferedWriter.write(
//                result.stream()
//                        .collect(joining("\n"))
//                        + "\n"
//        );

//        bufferedReader.close();
//        bufferedWriter.close();
    }
}


