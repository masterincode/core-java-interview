package interview.problems;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
class Car {
    private String carNo;
    private int power;
    private int torque;
}

class Test {

    public static void main(String[] args) {

        HashMap<Car, Car> map = new HashMap<>();
        Car c1 = Car.builder().carNo("A123").build();
        Car c2 = Car.builder().carNo("A123").build();
        Car c3 = Car.builder().carNo("A123").build();

        map.put(c1,c1);
        map.put(c1,c1);
        map.put(c2,c2);
        map.put(c3,c3);
        map.size();


    }
}


class HashMap<K, V> {
    private static final int DEFAULT_CAPACITY = 16;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    private Node<K, V>[] array;
    private int size;
    private int threshold;
    private float loadFactor;

    public HashMap() {
        this(DEFAULT_CAPACITY, DEFAULT_LOAD_FACTOR);
    }

    public HashMap(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }

    public HashMap(int initialCapacity, float loadFactor) {
        if (initialCapacity <= 0) {
            throw new IllegalArgumentException("Invalid initial capacity: " + initialCapacity);
        }
        if (loadFactor <= 0 || Float.isNaN(loadFactor)) {
            throw new IllegalArgumentException("Invalid load factor: " + loadFactor);
        }

        this.array = new Node[initialCapacity];
        this.size = 0;
        this.threshold = (int) (initialCapacity * loadFactor);
        this.loadFactor = loadFactor;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void put(K key, V value) {
        int hash = hash(key);
        int index = indexFor(hash, array.length);
//        int index = 1;

        //Q.   Why this For Loop is required inside put method????
        //Ans: This loop is required to iterate over Linked List at particular index
        for (Node<K, V> node = array[index]; node != null; node = node.next) {
            // 1. It compare hash value
            // 2. It compare key value
            // 3. If both are true then it update only value
            if (node.hash == hash && (key == node.key || key.equals(node.key))) {
                node.value = value; // Updating value when key is same
                return;
            }
        }

        addEntry(hash, key, value, index);
    }

    public V get(K key) {
        int hash = hash(key);
        int index = indexFor(hash, array.length);

        for (Node<K, V> node = array[index]; node != null; node = node.next) {
            if (node.hash == hash && (key == node.key || key.equals(node.key))) {
                return node.value;
            }
        }

        return null;
    }

    private void addEntry(int hash, K key, V value, int bucketIndex) {
        Node<K, V> head = array[bucketIndex]; // Assigning Head Node
        array[bucketIndex] = new Node<>(hash, key, value, head);

        if (size++ >= threshold) {
            resize(2 * array.length);
        }
    }

    private void resize(int newCapacity) {

        Node<K, V>[] oldTable = array;
        int oldCapacity = oldTable.length;

        if (oldCapacity == Integer.MAX_VALUE) {
            throw new IllegalStateException("Maximum size reached");
        }

        Node<K, V>[] newTable = new Node[newCapacity];
        transfer(oldTable, newTable);
        array = newTable;
        threshold = (int) (newCapacity * loadFactor);
    }

    private void transfer(Node<K, V>[] src, Node<K, V>[] dest) {
        for (Node<K, V> node : src) {
            while (node != null) {
                Node<K, V> next = node.next;
                int index = indexFor(node.hash, dest.length);
                node.next = dest[index];
                dest[index] = node;
                node = next;
            }
        }
    }

    private int hash(K key) {
        return key == null ? 0 : key.hashCode();
    }

    private int indexFor(int hash, int length) {
        return hash & (length - 1);
    }

    private static class Node<K, V> {
        private int hash;
        private K key;
        private V value;
        private Node<K, V> next;

        public Node(int hash, K key, V value, Node<K, V> node) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = node;
        }
    }
}




