package interview.exception;

import java.util.Arrays;
import java.util.List;

public class UnsupportedOperationException {

    public static void main(String[] args) {

        /*
		Step 1: In order to achieve UnsupportedOperationException use List.
		*/
        List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9);
        list.forEach(num -> {
            if(num % 2 == 0) {
                list.remove(num);
            }
        });
    }
}
