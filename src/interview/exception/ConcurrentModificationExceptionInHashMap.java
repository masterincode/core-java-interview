package interview.exception;

import java.util.HashMap;
import java.util.Map;

public class ConcurrentModificationExceptionInHashMap {

    public static void main(String[] args) {

        Map<Integer, Integer> map = new HashMap<>();
//		Map<Integer,Integer> map = new ConcurrentHashMap<>();

        //Here I prepared object with 10 values
        for (int i = 0; i < 10; i++) {
            map.put(i, i);
        }

		/*
		Step 1: In order to achieve ConcurrentModificationException use HashMap.
		Step 2: In order to avoid ConcurrentModificationException use ConcurrentHashMap.
		* */
        map.forEach((k, v) -> {
			System.out.println(map.remove(k,v));
        });
    }

}

