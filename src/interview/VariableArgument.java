package interview;

public class VariableArgument {
    public static void main(String[] args) {
        System.out.println("Length:"+ args.length);
        add(5);
        add(5,5);
        add(5,5,5);
        subtract("A");
        subtract("A","B");
        subtract("A","B","C");
    }
//    int... a  <==> int[] a
    public static void add(int... arr) {
        for(int value: arr)
          System.out.println(value);
          System.out.println("====completed=========");
    }

    public static void subtract(String... arr) {
        for(String value: arr)
            System.out.println(value);
        System.out.println("====completed=========");
    }


}
