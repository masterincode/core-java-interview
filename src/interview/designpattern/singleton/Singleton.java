package interview.designpattern.singleton;

import java.io.*;

/*
* How to prevent Singleton Pattern from Serialization and Cloning?
* https://www.geeksforgeeks.org/prevent-singleton-pattern-reflection-serialization-cloning/
*/
public class Singleton implements Serializable, Cloneable {

    public static Singleton instance = null;

    // private constructor
    private Singleton()
    {
        System.out.println("private constructor");
    }

    public static Singleton getInstance() {
        if(instance == null) {
            synchronized (Singleton.class) {
                if(instance == null) {
                    instance = new Singleton();
                    return instance;
                }
            }

        }
        return instance;
    }

    protected Object readResolve() {
        System.out.println("read resolve");
        return instance;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }
}
class GFG {

    public static void main(String[] args)
    {
        String filePath = "D:\\0_Google_Drive\\0_Udemy and Pluralsight 2020\\SpringBoot Projects IntelliJ\\core-java-interview\\File.txt";
        try {
            Singleton instance1 = Singleton.getInstance();
            ObjectOutput                     out = new ObjectOutputStream(new FileOutputStream(filePath));

            out.writeObject(instance1);
            out.close();

            // deserialize from file to object
            ObjectInput in                        = new ObjectInputStream(new FileInputStream(filePath));
            Singleton instance2  = (Singleton)in.readObject();
            in.close();

            System.out.println("instance1 hashCode:- "+ instance1.hashCode());
            System.out.println("instance2 hashCode:- "+ instance2.hashCode());

            // Cloning
            //=======================================================================================
            Singleton copy1 = Singleton.instance;
            Singleton copy2 = (Singleton)copy1.clone();

            System.out.println("copy1 hashCode:- " + copy1.hashCode());
            System.out.println("copy2 hashCode:- " + copy2.hashCode());
        }

        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
