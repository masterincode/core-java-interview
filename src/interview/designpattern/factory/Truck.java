package interview.designpattern.factory;

public class Truck implements Vehicle{
    @Override
    public void engineDetails() {
        System.out.println("Truck Engine Details !!!");
    }

}
