package interview.designpattern.factory;

public class Bike implements Vehicle{
    @Override
    public void engineDetails() {
        System.out.println("Bike Engine Details !!!");
    }
}
