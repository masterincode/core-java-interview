package interview.designpattern.factory;

public interface Vehicle {

    void engineDetails();
}
