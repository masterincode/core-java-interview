package interview.designpattern.factory;

import java.util.Scanner;

public class FactoryMethodMainMethod {

    //https://www.youtube.com/watch?v=JFvSCobD7JU
    public static void main(String[] args) {

        FactoryMethod factoryMethod =new FactoryMethod();
        System.out.println();
        System.out.println("Enter the Vehicle type ???");
        Vehicle vehicle = factoryMethod.getVehicleDetails(new Scanner(System.in).nextLine());
        if(vehicle != null)
          vehicle.engineDetails();
        else
            System.out.printf("Oops !! Vehicle not found");
        FactoryMethodMainMethod.main(null);
    }
}
