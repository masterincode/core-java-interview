package interview.designpattern.factory;

public class Car implements Vehicle{

    @Override
    public void engineDetails() {
        System.out.println("Car Engine Details !!!");
    }
}
