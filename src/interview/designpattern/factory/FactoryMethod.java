package interview.designpattern.factory;

public class FactoryMethod {

    public Vehicle getVehicleDetails(String vehicle) {
        if(vehicle == null)
            return null;
        if(vehicle.equalsIgnoreCase("Car"))
            return new Car();
        if(vehicle.equalsIgnoreCase("Bike"))
            return new Bike();
        if(vehicle.equalsIgnoreCase("Truck"))
            return new Truck();
        return null;
    }
}
