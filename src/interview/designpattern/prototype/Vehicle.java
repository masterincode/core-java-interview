package interview.designpattern.prototype;

import lombok.*;

@Builder
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Vehicle implements Cloneable{

    private String modelNumber;
    private String vehicleNumber;
    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }
}
