package interview.solid;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
class AccountDto {
    String accountHolderName;
    String accountNumber;
    AccountType accountType;

}

public class LiskovSubstitutionPrinciple {

    public static void main(String[] args) {
        FixedDepositAccount fixedDepositAccount = new FixedDepositAccount();
        fixedDepositAccount.totalAmount = 10000;

        SavingAccount savingAccount = new SavingAccount();
        savingAccount.totalAmount = 500;

        System.out.println(findAccountDetails(fixedDepositAccount));
        System.out.println(findAccountDetails(savingAccount));

        System.out.println(deposit(fixedDepositAccount));
        System.out.println(deposit(savingAccount));
    }

    public static AccountDto findAccountDetails(Account account) {
        return account.getAccountDetails();
    }

    public static ResponseType deposit(DepositAccount depositAccount) {
        return depositAccount.depositAmount(depositAccount);
    }
}


interface Account {
    AccountDto getAccountDetails();
}

abstract class DepositAccount implements Account {

    int totalAmount;
    public abstract ResponseType depositAmount(DepositAccount depositAccount);
}

@ToString
class FixedDepositAccount extends DepositAccount {

    private int totalFixedDepositAmount;

    @Override
    public AccountDto getAccountDetails() {
        return new AccountDto("Virat Kohli", "78945612356", AccountType.SAVING);
    }

    @Override
    public ResponseType depositAmount(DepositAccount depositAccount) {
        if(depositAccount.totalAmount != 0) {
            this.totalFixedDepositAmount = depositAccount.totalAmount;
            System.out.println("totalFixedDepositAmount: "+ totalFixedDepositAmount);
            return ResponseType.SUCCESS;
        } else
            return ResponseType.FAILED;
    }
}

@ToString
class SavingAccount extends DepositAccount {

    private int totalSavingAmount;

    @Override
    public AccountDto getAccountDetails() {
        return new AccountDto("Rohit Sharma", "70000012356", AccountType.SAVING);
    }

    @Override
    public ResponseType depositAmount(DepositAccount depositAccount) {
        if(depositAccount.totalAmount != 0) {
            this.totalSavingAmount = depositAccount.totalAmount;
            System.out.println("totalSavingAmount: "+ totalSavingAmount);
            return ResponseType.SUCCESS;
        } else
            return ResponseType.FAILED;
    }

}
enum AccountType {
    SAVING;
}

enum ResponseType {
    SUCCESS, FAILED;
}


