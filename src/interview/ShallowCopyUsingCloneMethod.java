package interview;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

public class ShallowCopyUsingCloneMethod {
    public static void main(String[] args) {

        MyBaby originalBaby = new MyBaby();
        try {
            // We clone the baby.
            MyBaby clonedBaby = (MyBaby) originalBaby.clone();
            System.out.println("originalBaby  => " + originalBaby);
            System.out.println("clonedBaby  => " + clonedBaby);
            System.out.println();
            // both babies now have: age 23, name "Dolly" and an empty arraylist
            originalBaby.setAge(30);
            // originalBaby has age 30, the clone has age 23
            originalBaby.setName("Molly");
            // same goes for the String, both are individual fields
            originalBaby.getList().add("Sachin");
            System.out.println("originalBaby=> " + originalBaby);
            System.out.println("clonedBaby  => " + clonedBaby);
            System.out.println();
            // both babies get the string added to the list,
            // because both point to the same list.
            clonedBaby.getList().add("Dhoni");
            clonedBaby.setName("BMS");
            clonedBaby.setAge(31);
            System.out.println("originalBaby=> " + originalBaby);
            System.out.println("clonedBaby  => " + clonedBaby);
            System.out.println();
            MyBaby clonedBaby2 = (MyBaby) originalBaby.clone();
            clonedBaby2.getList().remove(0);
            System.out.println("clonedBaby1  => " + clonedBaby);
            System.out.println("clonedBaby2  => " + clonedBaby2);
        } catch (CloneNotSupportedException e) {
        }
    }
}

@Data
class MyBaby implements Cloneable {

    private int age = 23;
    private String name = "Dolly";
    private List<String> list = new ArrayList<>();

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
