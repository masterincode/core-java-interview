package interview;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ReadFromTextFileBNP {

    /*  //BufferedReader
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String name = br.readLine();                // Reading input from STDIN
        System.out.println("Hi, " + name + ".");    // Writing output to STDOUT

        //Scanner
        Scanner s = new Scanner(System.in);
        String name = s.nextLine();                 // Reading input from STDIN
        System.out.println("Hi, " + name + ".");    // Writing output to STDOUT

    */

    public static void main(String[] args) throws IOException {
        File file = new File("D:\\0_Google_Drive\\0_Udemy and Pluralsight 2020\\SpringBoot Projects IntelliJ\\core-java-interview\\src\\ReadFile.txt");

        FileReader fileReader = new FileReader(file);
        int asciiNumber;
        String result = "";

        while ((asciiNumber = fileReader.read()) != -1) {
            char ch = (char) asciiNumber;
            result = result + ch;
        }
        System.out.println("String: "+result);
        getEmployeeRecord(result);

    }


    public static void getEmployeeRecord(String data) {

        String[] array = data.split("\r\n"); // To break string into new line wise
        System.out.println("Array: "+Arrays.toString(array));
        List<BnpEmployee> bnpEmployeeList = new ArrayList<>();
        BnpEmployee bnpEmployee;

        for(String row: array) {
            String[] dataSplit = row.split("\\|"); // To Split String by PIPE separated

            bnpEmployee = new BnpEmployee(Integer.valueOf(dataSplit[0]), dataSplit[1], dataSplit[2], Integer.valueOf(dataSplit[3]), Integer.valueOf(dataSplit[4]));
            bnpEmployeeList.add(bnpEmployee);
        }
        System.out.println("Before Sorting: "+ bnpEmployeeList);

        Comparator<BnpEmployee> comparator = (emp1, emp2) -> (emp1.getSalary() > emp2.getSalary()) ? -1 : (emp1.getSalary() < emp2.getSalary()) ? 1:
                (emp1.getDesignation() > emp2.getDesignation()) ? -1 : (emp1.getDesignation() < emp2.getDesignation()) ? 1: 0;

        List<BnpEmployee> result = bnpEmployeeList.stream().sorted(comparator).collect(Collectors.toList());
        System.out.println("After Sorting: "+result);


    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class BnpEmployee {

    private Integer employeeId;
    private String employeeName;
    private String location;
    private Integer designation;
    private Integer salary;


}