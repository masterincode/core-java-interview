package interview.collection;

import lombok.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ComparatorTesting {

	public static void main(String[] args) {
		
		List<LaptopClassModel> list = new ArrayList<>();
		list.add(new LaptopClassModel("Acer",4,8000));
		list.add(new LaptopClassModel("HP",16,12000));
		list.add(new LaptopClassModel("Dell",8,10000));
		
		Comparator<LaptopClassModel>  comparator = Comparator.comparing(LaptopClassModel::getBrand);

		Collections.sort(list,comparator);
		for(LaptopClassModel o :list )
		{
			System.out.println(o);
			/*  LaptopClassModel [brand=HP, ram=16, price=12000]
				LaptopClassModel [brand=Dell, ram=8, price=10000]
				LaptopClassModel [brand=Acer, ram=4, price=8000]
			 *
			 * */
		}
		

	}

}

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
class LaptopClassModel {
	private String brand;
	private int ram;
	private int price;
}





