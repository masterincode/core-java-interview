package interview.collection;

import interview.oops.encapsulation.Employee;

import java.util.TreeSet;

public class TreeSetUsingComparable {

	public static void main(String[] args) {
		
		TreeSet<Employee> tree = new TreeSet<>();
		
		tree.add(Employee.builder().name("David").age(74).salary(4500).build());
		tree.add(Employee.builder().name("Rohan").age(24).salary(6500).build());
		tree.add(Employee.builder().name("Banta").age(84).salary(3500).build());
		tree.add(Employee.builder().build());

			for(Employee emp : tree)
			{
				System.out.println(emp.toString());
			}	

	}

}
