package interview.collection;

import interview.oops.encapsulation.College;

import java.util.HashSet;


public class HashSetUsingComparable {

	public static void main(String[] args) {
		HashSet<College> tree = new HashSet<>();
		
		tree.add(new College("Satish",24, 25000f));
		tree.add(new College("David",74, 4500f));
		tree.add(new College("Satish",24, 25000f));
		tree.add(new College("Banta",84, 34000f));
		tree.add(new College("Chutiya",34, 55000f));
		
			for(College emp : tree)
			{
				System.out.println(emp.toString());
			}	

	}

}

