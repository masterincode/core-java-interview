package interview.collection;

import interview.oops.encapsulation.Employee;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class HashCodeAndEqualMethod
{
	 static public void  main(String[] args)
	{
		HashCodeAndEqualMethod mm= new HashCodeAndEqualMethod();
		mm.checking();
	}
	
	public void checking()
	{
	    Employee e1 = Employee.builder().build();
        Employee e2 = Employee.builder().build();
        System.out.println("e1: "+e1.hashCode());
        System.out.println("e2: "+e2.hashCode());


		Employee emp1= Employee.builder().name("a").age(19).salary(1000).build();
		Employee emp2= Employee.builder().name("a").age(19).salary(1000).build();
		Employee emp3= Employee.builder().name("a").age(18).salary(1000).build();

		Set<Employee> set = new HashSet<>();
		set.add(emp1);
		set.add(emp1);
		set.add(emp2);
		set.add(emp3);
		
 //     Note: Without overriding hashcode() and equal() 
		System.out.println(emp1.hashCode());  
		System.out.println(emp2.hashCode());
		System.out.println(emp3.hashCode());
		
		Map<Employee,String>  map=new HashMap<>();
		map.put(emp1, "Before"); //--hash code--2133927002
		map.put(emp1, "After"); //--hash code--2133927002
		map.put(emp2, "Balu"); //--hash code--1836019240
		map.put(emp3, "Bms"); //--hash code--325040804
		/*
		Note: Without overriding hashcode() and equal()
		 Size without overriding is 3
		---size of map---3
			---Key---Employee(id=0, name=a, salary=1000.0, age=19)    Value----Balu
			---Key---Employee(id=0, name=a, salary=1000.0, age=19)    Value----After
			---Key---Employee(id=0, name=a, salary=1000.0, age=18)    Value----Bms
		
		Note: With overriding hashcode() and equal()
		 Size without overriding is 2
		---size of map---2
			---Key---Employee(id=0, name=a, salary=1000.0, age=19)    Value----Balu
			---Key---Employee(id=0, name=a, salary=1000.0, age=18)    Value----Bms

		=============================Interview====================================
		Note: When Hash Collision will happen then only equals method get called otherwise not

		Use case 1: Over ride only hashcode() method
		Ans: During duplicate object, Hash Collision will happen then it will call the equals() method of object class
		     Object class equals() method is used for address check, since we have used new keyword it will behave every object as unique object
		     Therefore, Size will be 3

        Use case 2: Over ride only equals() method
        Ans: Employee object will generate unique hashcode every time, equals method will never get called.
             Therefore, Size will be 3

		Use case 3: Over ride hashcode() method and return 1 and equals() method will have proper logic
        Ans: Size will be 2

        Use case 4: Over ride hashcode() method and return 1 and equals() method will return true;
        Ans: Size will be 1

        Use case 5: Over ride hashcode() method and return 1 and equals() method will return false;
        Ans: Size will be 3

        Use case 6: Over ride hashcode() method with proper logic and equals() method will return false;
        Ans: Size will be 3

        Use case 7: Over ride hashcode() method with proper logic and equals() method will return true;
        Ans: Size will be 2
		 */

		System.out.println("---size of map---"+map.size()); 
		for(Map.Entry<Employee,String> m :map.entrySet())
		{
			System.out.println("---Key---"+m.getKey()+"    Value----"+m.getValue());
		}
		
		
	}
}

