package interview.collection;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArrayIntoArrayList {

	public static void main(String[] args) {
		
//		How to create number array directly
		int number[]={1,2,3,4,5,6,7,8};

//--------- Convert Array into List-----------------------------------------------
	   String cricketer[]= {"Sachin","Dravid","Rahul","Yuvraj","Amit","Bunty"};
		
//	   List<String> arrayToList= Arrays.asList(cricketer);
//								OR
    	List<String> arrayToList= Arrays.asList("Sachin","Dravid","Rahul","Yuvraj","Amit","Bunty");
	   System.out.println(arrayToList); //[Sachin, Dravid, Rahul, Yuvraj, Amit, Bunty]
	   Collections.sort(arrayToList);   // sort method is return type void
	   System.out.println(arrayToList); // [Amit, Bunty, Dravid, Rahul, Sachin, Yuvraj]
	   System.out.println();            //    0     1       2      3      4       5  

//--------- Convert List into Array-----------------------------------------------
		// 1. Using List toArray() method
        String[] arr = arrayToList.toArray(new String[0]);
		System.out.println(Arrays.toString(arr));

		// 2. Using Stream toArray() method
		String[] array = arrayToList.stream().toArray(String[]::new);
		System.out.println(Arrays.toString(array));
	}
}
