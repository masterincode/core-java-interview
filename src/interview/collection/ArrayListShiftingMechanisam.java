package interview.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListShiftingMechanisam {

    public static void main(String[] args) {
        Final finall = new Final();
        finall.addd();
        finall.removeOddNumberFromList();
    }
}

class Final {
    final static ArrayList<String> list1 = new ArrayList<>();
    static {
        list1.add("dhoni");
        list1.add("jadeja");
        list1.add("ajinkya");
        list1.add("dhawan");
        System.out.println(list1);  //OutPut: [dhoni, jadeja, ajinkya, dhawan]
    }

    public void addd() {

        //Tricky question--------------------------------------
        //After remvoing of element shifting will take place--------
        String a = list1.remove(0);
        System.out.println("--list1.remove(0)--shifitng will take place-");
        System.out.println(list1); //OutPut: [jadeja, ajinkya, dhawan]

        String b = list1.remove(1);
        System.out.println("--list1.remove(1)--shifitng will take place-");
//		list1.remove(3);
//		list1.remove(4);
        System.out.println(a); //dhoni
        System.out.println(b); //ajinkya
        System.out.println(list1); //Output: [jadeja, dhawan]
    }

    public void removeOddNumberFromList() {
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        list.removeIf(i -> i % 2 != 0);
        System.out.println(list);
    }

}
