package interview.collection;

import lombok.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ComparableTesting {

    public static void main(String[] args) {
        List<LaptopModel> list = new ArrayList<>();
        list.add(new LaptopModel("Acer", 4, 8000));
        list.add(new LaptopModel("HP", 16, 12000));
        list.add(new LaptopModel("Dell", 8, 10000));
        Collections.sort(list);
        for (LaptopModel o : list) {
            System.out.println(o);
        }
    }
}

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
class LaptopModel implements Comparable<LaptopModel> {
    private String brand;
    private int ram;
    private int price;

    @Override
    public int compareTo(LaptopModel model) {
        boolean abc = this.getPrice() > model.getPrice();
        if (abc)
            return -1; // desc order
        else
            return 1; // asc order
    }
}




