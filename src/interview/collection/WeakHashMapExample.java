package interview.collection;

import interview.oops.encapsulation.Employee;

import java.util.HashMap;
import java.util.WeakHashMap;

public class WeakHashMapExample {
    public static void main(String[] args) throws InterruptedException {

        Employee e1 =  new Employee();
        Employee e2 =  new Employee();

        // Test with HashMap
        HashMap<Employee, String> hashMap =  new HashMap<>();
        hashMap.put(e1,"first");
        e1 = null;
        System.gc();
        // thread sleeps for 4 sec
        Thread.sleep(1000);
        System.out.println(hashMap.size());

        WeakHashMap<Employee, String> weakHashMap =  new WeakHashMap<>();
        weakHashMap.put(e2,"first");
        e2 = null;
        System.gc();
        // thread sleeps for 4 sec
        Thread.sleep(1000);
        System.out.println(weakHashMap.size());
    }
}
