package interview;

import lombok.Builder;
import lombok.Data;

public class ClassObjectDeclaredFinal {

    public static void main(String args[]) {

        final CollegeStudent cs1 = CollegeStudent.builder().name("bms").rollno(10).build();
        final CollegeStudent cs2 = CollegeStudent.builder().name("abc").rollno(12).build();
        CollegeStudent cs3 = CollegeStudent.builder().name("abc").rollno(12).build();

        //You can reintialise Non-final object many time
        cs3 = new CollegeStudent("A", 10);
        cs3 = new CollegeStudent("B", 20); // This is current value

        //You cannot reinitialise final object, coz reinitialise with new keyword will give new address to reference variable
        //Since it is final object, you cannot provide new address
//		cs1 = new CollegeStudent("Alok",50); // Compile Time Error
//		cs1 = cs2; // Compile Time Error

        // But you can change their property value many times
        cs1.name = "bunty";
        cs1.rollno = 52;
    }
}

@Data
@Builder
class CollegeStudent {
    String name;
    int rollno;
}
